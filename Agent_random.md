

```python
import random
import numpy as np
import pandas as pd
```


```python

random.seed(40)

```


```python

#simple payoff matrix
payoff_a = [(1,-1),(-2,2)]
payoff_b = [(-1, 1),(2,-2)]
```


```python

class Agent:
    def __init__(O, payoff, name="agent"):
        O.payoff = payoff
        O.allmove = []
        #O.tirage = []
    def move(O):
        pass
        #a=np.random.randint(0,1)
        #O.tirage.append(a)
        #return a
    def take(O,a,b):
        #m=O.move()
        O.allmove.append(O.payoff[a][b])
        
    
    def set_move(O,strat):
        O.move=strat
```


```python

def at_random():
    if random.random()>0.5: a=1
    else: a=0
    return a
```



```python

def weight_random():
    if random.random()>0.6666666666666666 : a=1
    else: a=0
    return a
```


```python

a=Agent([(1,-1),(-2,2)], "A")
b=Agent([(-1, 1),(2,-2)],"B")
a.set_move(at_random)
b.set_move(at_random)

for i in range(0,30000):
    #a.move()
    m_a,m_b = a.move(),b.move()
    a.take(m_a,m_b)
    b.take(m_a,m_b)
    
_=plot(np.cumsum(a.allmove))
_=plot(np.cumsum(b.allmove))
```


![png](output_6_0.png)



```python

a=Agent([(1,-1),(-2,2)], "A")
b=Agent([(-1, 1),(2,-2)],"B")
a.set_move( lambda :  0)
b.set_move(lambda :  1)

for i in range(0,30000):
    #a.move()
    m_a,m_b = a.move(),b.move()
    a.take(m_a,m_b)
    b.take(m_a,m_b)
    
_=plot(np.cumsum(a.allmove),color="b")
_=plot(np.cumsum(b.allmove),color="r")

```



![png](output_7_0.png)



```python

a=Agent([(1,-1),(-2,2)], "A")
b=Agent([(-1, 1),(2,-2)],"B")
a.set_move( weight_random)
b.set_move(at_random)

for i in range(0,30000):
    #a.move()
    m_a,m_b = a.move(),b.move()
    a.take(m_a,m_b)
    b.take(m_a,m_b)
    
_=plot(np.cumsum(a.allmove),color="b")
_=plot(np.cumsum(b.allmove),color="r")
#a.move()
```




    0




![png](output_8_1.png)



```python

figsize(16,9)

for i in range(300):
    a=Agent([(1,-1),(-2,2)], "A")
    b=Agent([(-1, 1),(2,-2)],"B")
    a.set_move( weight_random)
    b.set_move(at_random)

    for i in range(0,3000):
        #a.move()
        m_a,m_b = a.move(),b.move()
        a.take(m_a,m_b)
        b.take(m_a,m_b)
    
    _=plot(np.cumsum(a.allmove), color='r',alpha=0.01)
    _=plot(np.cumsum(b.allmove),color='b', alpha=0.01)
    
```


![png](output_9_0.png)



```python

figsize(16,9)
for i in range(300):
    a=Agent([(1,-1),(-2,2)], "A")
    b=Agent([(-1, 1),(2,-2)],"B")
    a.set_move( at_random)
    b.set_move(at_random)

    for i in range(0,3000):
        #a.move()
        m_a,m_b = a.move(),b.move()
        a.take(m_a,m_b)
        b.take(m_a,m_b)
    
    _=plot(np.cumsum(a.allmove), alpha=0.01, color='r')
    _=plot(np.cumsum(b.allmove), alpha=0.01, color='b')
    
```


![png](output_10_0.png)

