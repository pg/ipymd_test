

[](./PDPS.png)


```python
from nacl.public import Box, PrivateKey, PublicKey
from nacl.signing import SigningKey, SignedMessage

from nacl.encoding import HexEncoder
from ecdsa import ellipticcurve, curves, util, numbertheory
import ecdsa as pyecdsa
import hashlib
import binascii
from binascii import unhexlify
import ecdsa as pyecdsa
from ecdsa.ellipticcurve import INFINITY

#SECP256k1 = pyecdsa.curves.find_curve((1, 3, 132, 0, 10))
SECP256k1 = pyecdsa.curves.find_curve((1, 3, 132, 0, 10))
```


```python


class Point(ellipticcurve.Point):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('order', SECP256k1.order)
        super(Point, self).__init__(SECP256k1.curve, *args, **kwargs)

    def __neg__(self):
      return Point( self.x(), -self.y())
   
    def __sub__(self, Q):
        a=self + -Q
        return Point(a.x(),a.y())

    
```


```python
priv_key= PrivateKey.generate()
signing_key=pyecdsa.SigningKey.generate( curve=SECP256k1)

```


```python
#range for statistical zero-knowledge property.
R=2**256 * 2**256 * 2**80
B=16# 2**256
K =16# 2**256
```


```python
def long_to_bytes (val, endianness='big'):

    # one (1) hex digit per four (4) bits
    width = val.bit_length()

    # unhexlify wants an even multiple of eight (8) bits, but we don't
    # want more digits than we need (hence the ternary-ish 'or')
    width += 8 - ((width % 8) or 8)

    # format width specifier: four (4) bits per hex digit
    fmt = '%%0%dx' % (width // 4)

    # prepend zero (0) to the width, to zero-pad the output
    s = unhexlify(fmt % val)

    if endianness == 'little':
        # see http://stackoverflow.com/a/931095/309233
        s = s[::-1]

    return s

```


```python
class Client(object):
    def __init__(O,key=None):
        if not key:
            O.k = signing_key=pyecdsa.SigningKey.generate( curve=SECP256k1)      
        else :
            O.k = key
    def file_process(O,path="./slice.1_8.fec"):
        f="A"*32#open(path).read()
        D=util.string_to_number(f)
        b=util.string_to_number(O.k.to_string())
  
        s=D%b
        n=D//b

        D_1=(n * b) + s
        r = long_to_bytes(rr)
    
        print hashlib.sha1(f).hexdigest()
        print hashlib.sha1(r).hexdigest()
    
        
        b_id =  SECP256k1.generator  * n
        ro1= SECP256k1.generator  * n
        ro2 = SECP256k1.generator  * s
            #private, public
        return ((n,b,s), (b_id, ro1,ro2))
        
    def generate_challenge1(O):
        return util.randrange(R)
    
    
    
```


```python
class server(object):
    def __init__(O, b_id, ro1,ro2): 
        O.ro1=ro1
        O.ro2=ro1
        O.file_pub=b_id
        
    def commitment(O, challenge):
        f="A"*32#open("./slice.1_8.fec").read()
        a=util.string_to_number(f)
        
        O.b=challenge
        O.z=a%b
        O.m=a//b
        O.r =  util.randrange(B**2)
        O.t = util.randrange(B**2)
        O.x1 = SECP256k1.generator * r
        O.x2 = SECP256k1.generator * t
    
        return ((O.x1,O.x2),(O.m,O.z,O.r,O.t))
    
```


```python
c=Client()
#s=server()
#priv_c,pub_c=c.file_process()

```


```python
def preprocessing():
    f=open("./slice.1_8.fec").read()
    D=util.string_to_number(f)
    b=util.string_to_number(signing_key.to_string())

    s=D%b
    n=D//b

    D_1=(n * b) + s
    #r = long_to_bytes(rr)
    
    print hashlib.sha1(f).hexdigest()
    #print hashlib.sha1(r).hexdigest()
    
        
    b_id =  SECP256k1.generator  * n
    ro1= SECP256k1.generator  * n
    ro2 = SECP256k1.generator  * s
    
    return ((n,b,s), (b_id, ro1,ro2))


def generate_challenge(n,b,s):
    
    b_id =  SECP256k1.generator  * n
    ro1= SECP256k1.generator  * n
    ro2 = SECP256k1.generator  * s
    
    return (b_id, ro1,ro2)


client_secret_priv, public_element = preprocessing()




```

    73ea3b5e061b6cabed623d1f0f8ba0e2dd7a8ef4



   [](./algo_keys.png)
```python
import ecdsa as pyecdsa

SECP256k1 = pyecdsa.curves.find_curve((1, 3, 132, 0, 10))
SECP256k1 = pyecdsa.curves.find_curve((1, 3, 132, 0, 10))

def generate_priv_challenge():
    f=open("./slice.1_8.fec").read()
    blockid= hashlib.sha256(signing_key.sign(f)).hexdigest()
    a=util.string_to_number(f)
    b=util.string_to_number(signing_key.to_string())#encode())
    s=a%b
    n=a//b
    ro1= SECP256k1.generator  * n
    ro2 = SECP256k1.generator  * s
    
    return (blockid, ro1,ro2,n,s)
    
```


```python
block_id,ro1,ro2,n,s = generate_priv_challenge()
```

[](./challenge.png)

```python
def remote_commitment(chall):
    f=open("./slice.1_8.fec").read()
    a=util.string_to_number(f)
    b=chall
    z=a%b
    m=a//b
    print z
    r =  util.randrange(B*B)
    t = util.randrange(B*B)
    #w= r^z
    #print w
    x1 = r * SECP256k1.generator 
    x2 = t * SECP256k1.generator 
    
    return ((x1,x2),(m,z,r,t))
    
    
```


```python
chall = util.randrange(R)

(commit,ch_resp)=commitment(chall)

```


```python
chall2 = util.randrange(K)
```


```python
def remote_resp(c, resp):
    m=resp[0]  
    z=resp[1]  
    r=resp[2]
    t=resp[3]
    
    y1= r+(c*z)
    y2= t+(c*m)
    
    return y1,y2
```


```python
y1,y2 = host_resp(chall2, ch_resp) 
```


```python
chall = util.randrange(R)
chall2 = util.randrange(K)

P = SECP256k1.generator


(commit,ch_resp)=remote_commitment(chall)
y1,y2 = remote_resp(chall2, ch_resp) 
x1=commit[0]
x2=commit[1]

b_ = chall
c = chall2

z =  ch_resp[1]
m = ch_resp[0]
```

    383769705759922238814820635125091393536751781330499963960027267106043604344928356482304970868177244815080981113707959740583852333897030317262689421782904643950974779849213407220


[](./proof.png)

```python
as1_1= ( y1 * P ) + (-Point(x1.x(),x1.y()))#y1.P-x1
as1_2= ( c * z  ) *  P #cz.P
print "assert I", as1_1==as1_2
print as1_1.x()
print as1_2.x()
as2_1=( y2 * P ) + ( -Point(x2.x(),x2.y()) )
as2_2= (b_* m )  * P
print "assert II", as2_1==as2_2
print as2_1.x()
print as2_2.x()
```

    assert I True
    5876730895232555975670752555686052848272136747449052900777052125475456965293
    5876730895232555975670752555686052848272136747449052900777052125475456965293
    assert II False
    95958819438731652205001323648120835610712117261911186719618043262918945418931
    55940851260522358385586042305550878175039129341223631825528624425777497241555



```python
as2_1=(y2 * P) + (-Point(x2.x(),x2.y()))
as2_2= (b_*m) * P
print as2_1.x()
print as2_2.x()

```

    91728786092928355630891141238137171644308836139219436085412195700189316134122
    107279711877694500410709767952754959809170673439664368886384200698769658470714



<img src='./equality.png'>


```python
SK=b=util.string_to_number(signing_key.to_string())
chall*SK * ro1 - chall2 * (y2 * SECP256k1.generator + - Point(commit[0].x(),commit[0].y())
```




    <ecdsa.ellipticcurve.Point at 0x106e16f50>




```python
%qtconsole
```


```python
x1=commit[0]
Y1=y1*SECP256k1.generator
Y1=Point(Y1.x(),Y1.y())
RO1=chall2*Point(ro1.x(),ro1.y())
X1=Point(x1.x(),x1.y())
RO1=Point(RO1.x(),RO1.y())

assertion1 = Y1-X1-RO1
```


```python
ro1.y()
```


```python
sk=util.string_to_number(signing_key.to_string())

curv=SECP256k1.curve

#chall2*sk*ro2 - b*(y2.Gen-x2)
tb1=chall2*sk*ro2

x2=commit[1]
#x2=Point(x2.x(),x2.y())
Y2=SECP256k1.generator * y2
Y2=Point(Y2.x(),Y2.y())
X2=Point(x2.x(),x2.y())
XY2=Y2-X2
XY2 = - chall * XY2

assertion2 = tb1 + XY2
#Y3=Point(Y2.x(),Y2.y())

#chall*(Y2-x2)
#tb2 = chall*((SECP256k1.generator * y2) + invx2)
#Z2=Point(tb2.x(),-tb2.y())

#assertion2=tb1+Z2


#chall
```


```python
print assertion1==assertion2
print assertion1.x() 
print assertion2.x()
#Point(t1.x(),- t1.y())-Point(t1.x(),t1.y())
```

    False
    7736584874372799755670583036719701680498164259870020736174578513076009517175
    26299729680235291815366724209282966839464132792531995892755583725140232542577

