

```
import pandas as pd
import redis
import pickle

```


```
r=redis.Redis(port=6666)
#r=redis.Redis()
```


```

i=0

f={}
for st in r.lrange("strat_list",0,100):
                    try:   
                        #if st != 'MACROSS_2_20':  
                            s=pd.DataFrame([pickle.loads(i) 
                                for i in r.lrange('%s:positions'%st,0,100000)])
 
                            s.reset_index()
                            s.index=s.date
                            s=s.sort_index()

                            s=s[s.status=="filled"]#.fillna(0)
                            
                            s.pnl.fillna(0,inplace=True)
                            s.side.fillna(method='ffill',inplace=True)
                            
                            f[st]=(s.pnl).cumsum()
                            print 
                            print "profit/loss", st,((s[s.status=="filled"].pnl)).sum()#.abs()

                    except Exception, e :
                            print st, e
                            pass#print e# '%s : P/L:%f PnL:%f %d transactions'%(st,0,0,0)


```

    
    profit/loss CCI 0.364493427108
    
    profit/loss Volume_inbal 0.0311087466221
    
    profit/loss VHMM_21_205 0.0892311999016
    
    profit/loss VHMM_21_124 0.0269838540813
    
    profit/loss VHMM_50_208 0.0777806134005
    
    profit/loss MACROSS_2_20 0.247519265629
    
    profit/loss MACROSS_21_15 0.0230190248993



```
for k in f.keys():
    #if k != 'MACROSS_2_20':
        f[k].plot(figsize=(15,8))
legend( f.keys(), "best")

```




    <matplotlib.legend.Legend at 0x118459210>




![png](output_3_1.png)



```
st="Volume_inbal"
s=pd.DataFrame([pickle.loads(i) 
                                for i in r.lrange('%s:positions'%st,0,100000)])
s.reset_index()
s.index=s.date
s=s.sort_index()
```


```
%qtconsole
```


```
s[(s.status=='filled') & (s.index.duplicated() == True)]
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>date</th>
      <th>exit</th>
      <th>fill_time</th>
      <th>order_type</th>
      <th>pnl</th>
      <th>price</th>
      <th>side</th>
      <th>status</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2015-01-20 19:11:00</th>
      <td>2015-01-20 19:11:00</td>
      <td> 0</td>
      <td>2015-01-20 19:26:00</td>
      <td> limit</td>
      <td>-0.000970</td>
      <td> 1298.50</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 19:48:00</th>
      <td>2015-01-20 19:48:00</td>
      <td> 0</td>
      <td>2015-01-21 02:06:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1301.82</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 21:31:00</th>
      <td>2015-01-20 21:31:00</td>
      <td> 0</td>
      <td>2015-01-20 21:32:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.63</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 21:39:00</th>
      <td>2015-01-20 21:39:00</td>
      <td> 0</td>
      <td>2015-01-20 21:40:00</td>
      <td> limit</td>
      <td> 0.002974</td>
      <td> 1291.10</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:13:00</th>
      <td>2015-01-20 22:13:00</td>
      <td> 0</td>
      <td>2015-01-20 22:15:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1286.29</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:19:00</th>
      <td>2015-01-20 22:19:00</td>
      <td> 0</td>
      <td>2015-01-20 22:22:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.50</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:29:00</th>
      <td>2015-01-20 22:29:00</td>
      <td> 0</td>
      <td>2015-01-20 22:30:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.60</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:31:00</th>
      <td>2015-01-20 22:31:00</td>
      <td> 0</td>
      <td>2015-01-20 22:34:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.29</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:41:00</th>
      <td>2015-01-20 22:41:00</td>
      <td> 0</td>
      <td>2015-01-20 22:43:00</td>
      <td> limit</td>
      <td> 0.000016</td>
      <td> 1288.32</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:49:00</th>
      <td>2015-01-20 22:49:00</td>
      <td> 0</td>
      <td>2015-01-20 22:50:00</td>
      <td> limit</td>
      <td> 0.000536</td>
      <td> 1286.90</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:03:00</th>
      <td>2015-01-20 23:03:00</td>
      <td> 0</td>
      <td>2015-01-20 23:04:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.69</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:08:00</th>
      <td>2015-01-20 23:08:00</td>
      <td> 0</td>
      <td>2015-01-20 23:09:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1294.60</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:09:00</th>
      <td>2015-01-20 23:09:00</td>
      <td> 0</td>
      <td>2015-01-20 23:11:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:24:00</th>
      <td>2015-01-20 23:24:00</td>
      <td> 0</td>
      <td>2015-01-20 23:30:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:33:00</th>
      <td>2015-01-20 23:33:00</td>
      <td> 0</td>
      <td>2015-01-20 23:34:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1292.98</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:33:00</th>
      <td>2015-01-20 23:33:00</td>
      <td> 0</td>
      <td>2015-01-20 23:35:00</td>
      <td> limit</td>
      <td>-0.000008</td>
      <td> 1292.01</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:37:00</th>
      <td>2015-01-20 23:37:00</td>
      <td> 0</td>
      <td>2015-01-20 23:39:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:39:00</th>
      <td>2015-01-20 23:39:00</td>
      <td> 0</td>
      <td>2015-01-20 23:40:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.31</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:57:00</th>
      <td>2015-01-20 23:57:00</td>
      <td> 0</td>
      <td>2015-01-20 23:58:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.36</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:57:00</th>
      <td>2015-01-20 23:57:00</td>
      <td> 0</td>
      <td>2015-01-21 00:39:00</td>
      <td> limit</td>
      <td> 0.000333</td>
      <td> 1292.02</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:00:00</th>
      <td>2015-01-21 00:00:00</td>
      <td> 0</td>
      <td>2015-01-21 00:01:00</td>
      <td> limit</td>
      <td>-0.000193</td>
      <td> 1293.91</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:01:00</th>
      <td>2015-01-21 00:01:00</td>
      <td> 0</td>
      <td>2015-01-21 00:02:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1294.76</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:12:00</th>
      <td>2015-01-21 00:12:00</td>
      <td> 0</td>
      <td>2015-01-21 00:13:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1294.85</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:26:00</th>
      <td>2015-01-21 00:26:00</td>
      <td> 0</td>
      <td>2015-01-21 00:28:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1294.99</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:29:00</th>
      <td>2015-01-21 00:29:00</td>
      <td> 0</td>
      <td>2015-01-21 00:30:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:30:00</th>
      <td>2015-01-21 00:30:00</td>
      <td> 0</td>
      <td>2015-01-21 00:31:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:40:00</th>
      <td>2015-01-21 00:40:00</td>
      <td> 0</td>
      <td>2015-01-21 00:41:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.80</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:41:00</th>
      <td>2015-01-21 00:41:00</td>
      <td> 0</td>
      <td>2015-01-21 00:43:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:45:00</th>
      <td>2015-01-21 00:45:00</td>
      <td> 0</td>
      <td>2015-01-21 00:46:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.91</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:50:00</th>
      <td>2015-01-21 00:50:00</td>
      <td> 0</td>
      <td>2015-01-21 01:01:00</td>
      <td> limit</td>
      <td>-0.003326</td>
      <td> 1293.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:53:00</th>
      <td>2015-01-21 00:53:00</td>
      <td> 0</td>
      <td>2015-01-21 00:54:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.48</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:56:00</th>
      <td>2015-01-21 00:56:00</td>
      <td> 0</td>
      <td>2015-01-21 00:58:00</td>
      <td> limit</td>
      <td> 0.000054</td>
      <td> 1293.27</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:08:00</th>
      <td>2015-01-21 01:08:00</td>
      <td> 0</td>
      <td>2015-01-21 01:09:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1290.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:10:00</th>
      <td>2015-01-21 01:10:00</td>
      <td> 0</td>
      <td>2015-01-21 01:32:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1290.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:13:00</th>
      <td>2015-01-21 01:13:00</td>
      <td> 0</td>
      <td>2015-01-21 01:14:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1291.92</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:30:00</th>
      <td>2015-01-21 01:30:00</td>
      <td> 0</td>
      <td>2015-01-21 01:33:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1293.24</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:35:00</th>
      <td>2015-01-21 01:35:00</td>
      <td> 0</td>
      <td>2015-01-21 01:37:00</td>
      <td> limit</td>
      <td> 0.000874</td>
      <td> 1292.25</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:05:00</th>
      <td>2015-01-21 02:05:00</td>
      <td> 0</td>
      <td>2015-01-21 02:08:00</td>
      <td> limit</td>
      <td> 0.003692</td>
      <td> 1300.20</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:13:00</th>
      <td>2015-01-21 02:13:00</td>
      <td> 0</td>
      <td>2015-01-21 02:23:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1309.90</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:30:00</th>
      <td>2015-01-21 02:30:00</td>
      <td> 0</td>
      <td>2015-01-21 02:31:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1304.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:31:00</th>
      <td>2015-01-21 02:31:00</td>
      <td> 0</td>
      <td>2015-01-21 02:33:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1303.78</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:35:00</th>
      <td>2015-01-21 02:35:00</td>
      <td> 0</td>
      <td>2015-01-21 02:37:00</td>
      <td> limit</td>
      <td> 0.000038</td>
      <td> 1300.63</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:48:00</th>
      <td>2015-01-21 02:48:00</td>
      <td> 0</td>
      <td>2015-01-21 02:58:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1301.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 03:02:00</th>
      <td>2015-01-21 03:02:00</td>
      <td> 0</td>
      <td>2015-01-21 03:10:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.68</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 03:06:00</th>
      <td>2015-01-21 03:06:00</td>
      <td> 0</td>
      <td>2015-01-21 03:07:00</td>
      <td> limit</td>
      <td>-0.000363</td>
      <td> 1295.59</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 03:18:00</th>
      <td>2015-01-21 03:18:00</td>
      <td> 0</td>
      <td>2015-01-21 03:19:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1300.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
  </tbody>
</table>
</div>




```
name='VHMM_50_208'
zz=pickle.loads(r.get('strategy:%s'%name))
```


```
zz[zz.signal.diff()!=0]
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>signal</th>
      <th>price</th>
      <th>returns</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2015-01-21 19:21:00</th>
      <td> 0</td>
      <td> 1324.20</td>
      <td>      NaN</td>
    </tr>
    <tr>
      <th>2015-01-21 23:25:00</th>
      <td> 1</td>
      <td> 1396.86</td>
      <td>-0.000136</td>
    </tr>
    <tr>
      <th>2015-01-22 00:14:00</th>
      <td> 0</td>
      <td> 1403.35</td>
      <td>-0.000783</td>
    </tr>
  </tbody>
</table>
</div>




```
ww.pnl.fillna(0,inplace=True)
```


```
ww.pnl.cumsum().plot()
```




    <matplotlib.axes.AxesSubplot at 0x109de1c50>




![png](output_10_1.png)



```
s[s.index.duplicated()==True]
```




<div style="max-height:1000px;max-width:1500px;overflow:auto;">
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>date</th>
      <th>exit</th>
      <th>fill_time</th>
      <th>order_type</th>
      <th>pnl</th>
      <th>price</th>
      <th>side</th>
      <th>status</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2015-01-20 19:11:00</th>
      <td>2015-01-20 19:11:00</td>
      <td> 0</td>
      <td>2015-01-20 19:26:00</td>
      <td> limit</td>
      <td>-0.000970</td>
      <td> 1298.50</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 19:48:00</th>
      <td>2015-01-20 19:48:00</td>
      <td> 0</td>
      <td>2015-01-21 02:06:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1301.82</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 21:29:00</th>
      <td>2015-01-20 21:29:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.88</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-20 21:31:00</th>
      <td>2015-01-20 21:31:00</td>
      <td> 0</td>
      <td>2015-01-20 21:32:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.63</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 21:31:00</th>
      <td>2015-01-20 21:31:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.87</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-20 21:39:00</th>
      <td>2015-01-20 21:39:00</td>
      <td> 0</td>
      <td>2015-01-20 21:40:00</td>
      <td> limit</td>
      <td> 0.002974</td>
      <td> 1291.10</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 21:49:00</th>
      <td>2015-01-20 21:49:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1294.56</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-20 22:13:00</th>
      <td>2015-01-20 22:13:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1286.10</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-20 22:13:00</th>
      <td>2015-01-20 22:13:00</td>
      <td> 0</td>
      <td>2015-01-20 22:15:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1286.29</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:19:00</th>
      <td>2015-01-20 22:19:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1288.92</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-20 22:19:00</th>
      <td>2015-01-20 22:19:00</td>
      <td> 0</td>
      <td>2015-01-20 22:22:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.50</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:29:00</th>
      <td>2015-01-20 22:29:00</td>
      <td> 0</td>
      <td>2015-01-20 22:30:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.60</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:31:00</th>
      <td>2015-01-20 22:31:00</td>
      <td> 0</td>
      <td>2015-01-20 22:34:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1289.29</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:41:00</th>
      <td>2015-01-20 22:41:00</td>
      <td> 0</td>
      <td>2015-01-20 22:43:00</td>
      <td> limit</td>
      <td> 0.000016</td>
      <td> 1288.32</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 22:49:00</th>
      <td>2015-01-20 22:49:00</td>
      <td> 0</td>
      <td>2015-01-20 22:50:00</td>
      <td> limit</td>
      <td> 0.000536</td>
      <td> 1286.90</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:03:00</th>
      <td>2015-01-20 23:03:00</td>
      <td> 0</td>
      <td>2015-01-20 23:04:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.69</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:08:00</th>
      <td>2015-01-20 23:08:00</td>
      <td> 0</td>
      <td>2015-01-20 23:09:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1294.60</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:09:00</th>
      <td>2015-01-20 23:09:00</td>
      <td> 0</td>
      <td>2015-01-20 23:11:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:24:00</th>
      <td>2015-01-20 23:24:00</td>
      <td> 0</td>
      <td>2015-01-20 23:30:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:33:00</th>
      <td>2015-01-20 23:33:00</td>
      <td> 0</td>
      <td>2015-01-20 23:34:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1292.98</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:33:00</th>
      <td>2015-01-20 23:33:00</td>
      <td> 0</td>
      <td>2015-01-20 23:35:00</td>
      <td> limit</td>
      <td>-0.000008</td>
      <td> 1292.01</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:37:00</th>
      <td>2015-01-20 23:37:00</td>
      <td> 0</td>
      <td>2015-01-20 23:39:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:39:00</th>
      <td>2015-01-20 23:39:00</td>
      <td> 0</td>
      <td>2015-01-20 23:40:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.31</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:57:00</th>
      <td>2015-01-20 23:57:00</td>
      <td> 0</td>
      <td>2015-01-20 23:58:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.36</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-20 23:57:00</th>
      <td>2015-01-20 23:57:00</td>
      <td> 0</td>
      <td>2015-01-21 00:39:00</td>
      <td> limit</td>
      <td> 0.000333</td>
      <td> 1292.02</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:00:00</th>
      <td>2015-01-21 00:00:00</td>
      <td> 0</td>
      <td>2015-01-21 00:01:00</td>
      <td> limit</td>
      <td>-0.000193</td>
      <td> 1293.91</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:01:00</th>
      <td>2015-01-21 00:01:00</td>
      <td> 0</td>
      <td>2015-01-21 00:02:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1294.76</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:01:00</th>
      <td>2015-01-21 00:01:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.59</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 00:12:00</th>
      <td>2015-01-21 00:12:00</td>
      <td> 0</td>
      <td>2015-01-21 00:13:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1294.85</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:26:00</th>
      <td>2015-01-21 00:26:00</td>
      <td> 0</td>
      <td>2015-01-21 00:28:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1294.99</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2015-01-21 00:30:00</th>
      <td>2015-01-21 00:30:00</td>
      <td> 0</td>
      <td>2015-01-21 00:31:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:40:00</th>
      <td>2015-01-21 00:40:00</td>
      <td> 0</td>
      <td>2015-01-21 00:41:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.80</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:41:00</th>
      <td>2015-01-21 00:41:00</td>
      <td> 0</td>
      <td>2015-01-21 00:43:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.14</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:45:00</th>
      <td>2015-01-21 00:45:00</td>
      <td> 0</td>
      <td>2015-01-21 00:46:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.91</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:50:00</th>
      <td>2015-01-21 00:50:00</td>
      <td> 0</td>
      <td>2015-01-21 01:01:00</td>
      <td> limit</td>
      <td>-0.003326</td>
      <td> 1293.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:53:00</th>
      <td>2015-01-21 00:53:00</td>
      <td> 0</td>
      <td>2015-01-21 00:54:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1293.48</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 00:56:00</th>
      <td>2015-01-21 00:56:00</td>
      <td> 0</td>
      <td>2015-01-21 00:58:00</td>
      <td> limit</td>
      <td> 0.000054</td>
      <td> 1293.27</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:08:00</th>
      <td>2015-01-21 01:08:00</td>
      <td> 0</td>
      <td>2015-01-21 01:09:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1290.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:10:00</th>
      <td>2015-01-21 01:10:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1290.00</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 01:10:00</th>
      <td>2015-01-21 01:10:00</td>
      <td> 0</td>
      <td>2015-01-21 01:32:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1290.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:13:00</th>
      <td>2015-01-21 01:13:00</td>
      <td> 0</td>
      <td>2015-01-21 01:14:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1291.92</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:22:00</th>
      <td>2015-01-21 01:22:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1292.98</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 01:30:00</th>
      <td>2015-01-21 01:30:00</td>
      <td> 0</td>
      <td>2015-01-21 01:33:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1293.24</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:35:00</th>
      <td>2015-01-21 01:35:00</td>
      <td> 0</td>
      <td>2015-01-21 01:37:00</td>
      <td> limit</td>
      <td> 0.000874</td>
      <td> 1292.25</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 01:57:00</th>
      <td>2015-01-21 01:57:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1296.15</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 02:03:00</th>
      <td>2015-01-21 02:03:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1295.64</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 02:05:00</th>
      <td>2015-01-21 02:05:00</td>
      <td> 0</td>
      <td>2015-01-21 02:08:00</td>
      <td> limit</td>
      <td> 0.003692</td>
      <td> 1300.20</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:13:00</th>
      <td>2015-01-21 02:13:00</td>
      <td> 0</td>
      <td>2015-01-21 02:23:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1309.90</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:14:00</th>
      <td>2015-01-21 02:14:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1311.10</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 02:30:00</th>
      <td>2015-01-21 02:30:00</td>
      <td> 0</td>
      <td>2015-01-21 02:31:00</td>
      <td> limit</td>
      <td>-0.000000</td>
      <td> 1304.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:31:00</th>
      <td>2015-01-21 02:31:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1303.51</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 02:31:00</th>
      <td>2015-01-21 02:31:00</td>
      <td> 0</td>
      <td>2015-01-21 02:33:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1303.78</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:35:00</th>
      <td>2015-01-21 02:35:00</td>
      <td> 0</td>
      <td>2015-01-21 02:37:00</td>
      <td> limit</td>
      <td> 0.000038</td>
      <td> 1300.63</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:39:00</th>
      <td>2015-01-21 02:39:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1299.96</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 02:48:00</th>
      <td>2015-01-21 02:48:00</td>
      <td> 0</td>
      <td>2015-01-21 02:58:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1301.00</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 02:52:00</th>
      <td>2015-01-21 02:52:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1304.57</td>
      <td> 0</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 03:02:00</th>
      <td>2015-01-21 03:02:00</td>
      <td> 0</td>
      <td>2015-01-21 03:10:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1298.68</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 03:06:00</th>
      <td>2015-01-21 03:06:00</td>
      <td> 0</td>
      <td>2015-01-21 03:07:00</td>
      <td> limit</td>
      <td>-0.000363</td>
      <td> 1295.59</td>
      <td> 1</td>
      <td> filled</td>
    </tr>
    <tr>
      <th>2015-01-21 03:09:00</th>
      <td>2015-01-21 03:09:00</td>
      <td> 0</td>
      <td>                NaT</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1297.88</td>
      <td> 1</td>
      <td>   open</td>
    </tr>
    <tr>
      <th>2015-01-21 03:18:00</th>
      <td>2015-01-21 03:18:00</td>
      <td> 0</td>
      <td>2015-01-21 03:19:00</td>
      <td> limit</td>
      <td> 0.000000</td>
      <td> 1300.00</td>
      <td> 0</td>
      <td> filled</td>
    </tr>
  </tbody>
</table>
<p>61 rows × 8 columns</p>
</div>




```

```
